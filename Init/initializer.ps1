﻿param(
    $Actions,
    [string]$FeedAPIKey,
    [string]$FeedUrl
)

$ErrorActionPreference = "Stop"

# Private
[bool]$Local = $true
[string]$RootPath = (Get-Item $PSScriptRoot).Parent | % { $_.FullName }
[string]$AssetPath = "$RootPath\Assets"
# Myget Environment Variables
[string]$BuildRunner = "Local"
[string]$SourcesPath = $null
[string]$Configuration = "Release"
[string]$Platform = $null
[string]$Targets = "Rebuild"
[string]$VersionFormat = $null
[string]$BuildCounter = $null
[string]$PackageVersion = $null
[string]$GallioEcho = $null
[string]$XUnit192Path = $null
[string]$XUnit20Path = $null
[string]$VsTestConsole = $null
[bool]$EnableNuGetPackageRestore = $true

# Initialize variables based on Environment, which can be Local or Remote build service(myget)
function Check-Environment
{
    # http://docs.myget.org/docs/reference/build-services#Available_Environment_Variables

    If((Test-Path Env:\BuildRunner) -eq $true)   
    {  
        Write-Host $env:BuildRunner
        $Script:BuildRunner = $env:BuildRunner
        $Script:Local = $false
    }

    If((Test-Path Env:\SourcesPath) -eq $true)   {  $Script:SourcesPath = $env:SourcesPath  }
    If((Test-Path Env:\Configuration) -eq $true)   {  $Script:Configuration = $env:Configuration  }
    If((Test-Path Env:\Platform) -eq $true)   {  $Script:Platform = $env:Platform  }
    If((Test-Path Env:\Targets) -eq $true)   {  $Script:Targets = $env:Targets  }
    If((Test-Path Env:\VersionFormat) -eq $true)   {  $Script:VersionFormat = $env:VersionFormat  }
    If((Test-Path Env:\BuildCounter) -eq $true)   {  $Script:BuildCounter = $env:BuildCounter  }
    If((Test-Path Env:\PackageVersion) -eq $true)   {  $Script:PackageVersion = $env:PackageVersion  }
    If((Test-Path Env:\EnableNuGetPackageRestore) -eq $true)   {  $Script:EnableNuGetPackageRestore = $env:EnableNuGetPackageRestore  }
    If((Test-Path Env:\GallioEcho) -eq $true)   {  $Script:GallioEcho = $env:GallioEcho  }
    If((Test-Path Env:\XUnit192Path) -eq $true)   {  $Script:XUnit192Path = $env:XUnit192Path  }
    If((Test-Path Env:\XUnit20Path) -eq $true)   {  $Script:XUnit20Path = $env:XUnit20Path  }
    If((Test-Path Env:\VsTestConsole) -eq $true)   {  $Script:VsTestConsole = $env:VsTestConsole  }
}

function Create-Directory
{
    # Check if folder exists
    if(!(Test-Path $AssetPath -pathType container))
    {
        # Create folder if not
        New-Item -ItemType directory -Path $AssetPath
    }
}

function Information
{
    Write-Host ""
    Write-Host "--- Initializer Parameters---"
    foreach($Action in $Actions) {
        Write-Host "Mode: $($Action.Mode)"
        Write-Host "PublishProfile: $($Action.PublishProfile)"
        Write-Host "SolutionFilter: $($Action.SolutionFilter)"
        Write-Host "NuspecFilter: $($Action.NuspecFilter)"
        foreach($Script in $Action.Scripts)
        {
            Write-Host "Scripts: $Script"
        }
    }
    Write-Host "FeedAPIKey: $FeedAPIKey"
    Write-Host "FeedUrl: $FeedUrl"
    Write-Host "MyGet"
    Write-Host "BuildRunner: $BuildRunner"
    Write-Host "SourcesPath: $SourcesPath"
    Write-Host "Configuration: $Configuration"
    Write-Host "Platform: $Platform"
    Write-Host "Targets: $Targets"
    Write-Host "VersionFormat: $VersionFormat"
    Write-Host "BuildCounter: $BuildCounter"
    Write-Host "PackageVersion: $PackageVersion"
    Write-Host "GallioEcho: $GallioEcho"
    Write-Host "XUnit192Path: $XUnit192Path"
    Write-Host "XUnit20Path: $XUnit20Path"
    Write-Host "VsTestConsole: $VsTestConsole"
    Write-Host "EnableNuGetPackageRestore: $EnableNuGetPackageRestore"
    Write-Host ""
    Write-Host "--- Initializer Data ---"
    Write-Host "Local: $Local"
    Write-Host "RootPath: $RootPath"
    Write-Host "AssetPath: $AssetPath"
    Write-Host ""

    Git-Information
    Write-Host ""

    Compiler-Information
    Write-Host ""

    NuGet-Information
    Write-Host ""

}

function Copy-ActionFiles
{
    param(
        $Files
    )
    foreach($File in $Files)
    {
        if(Test-Path $File.Source -PathType Container)
        {
            Copy-Item -Path $File.Source -Destination $File.Target -Recurse
        }
        else
        {
            # Make sure directory structure is created before copying.
            New-Item -ItemType File -Path $File.Target -Force
            Copy-Item -Path $File.Source -Destination $File.Target
        }
    }
}

function Init
{
    param(
        [boolean]$Local
    )
    if($Local -eq $true)
    {
        $Script:SourcesPath = $(Git-Root)
    }
}

try
{
    Write-Host "Creating directory's..."
    Create-Directory

    Write-Host "Checking environment..."
    Check-Environment

    Write-Host "Removing modules..."
    Remove-Module "utility" -ErrorAction SilentlyContinue
    Remove-Module "nuget" -ErrorAction SilentlyContinue
    Remove-Module "compiler" -ErrorAction SilentlyContinue
    Remove-Module "git" -ErrorAction SilentlyContinue

    Write-Host "Importing modules..."
    Import-Module "$RootPath\Modules\utility.psm1" -WarningAction SilentlyContinue
    Import-Module "$RootPath\Modules\nuget.psm1" -ArgumentList $Local, $AssetPath, $RootPath, $FeedAPIKey, $FeedUrl -WarningAction SilentlyContinue
    Import-Module "$RootPath\Modules\compiler.psm1" -ArgumentList $Local, $AssetPath, $RootPath -WarningAction SilentlyContinue
    Import-Module "$RootPath\Modules\git.psm1" -ArgumentList $Local, $AssetPath, $RootPath -WarningAction SilentlyContinue

    Write-Host "Initiating..."
    Init -Local $Local

    Write-Host "Listing script information:"
    Information
   
    Write-Host "Running actions..."
    foreach($Action in $Actions) {

        Write-Host "Mode: $($Action.Mode)"

        if($Action.Files)
        {
            Write-Host "Copying provided files from source to destination..."
            foreach($File in $Action.Files)
            {
                if(Test-Path $File.Source -PathType Container)
                {
                    Copy-Item -Path $File.Source -Destination $File.Target -Recurse
                }
                else
                {
                    # Make sure directory structure is created before copying.
                    New-Item -ItemType File -Path $File.Target -Force
                    Copy-Item -Path $File.Source -Destination $File.Target
                }
            }
            Write-Host ""
        }

        switch ($Action.Mode)
        {
            0 {
                Read-Host "Error: Please setup build file for project. Press Enter to continue"
            }
            1 {
                NuGet-Push
            }
            # Version -> Restore -> Build -> Package -> Restore Git
            2 { 
                $solutionPath = Utility-SearchFile -Path $SourcesPath -Filter $Action.SolutionFilter
                $version = Git-Version -SourcesPath $SourcesPath -UpdateAssemblyInfo "true"
                NuGet-Restore -Path $solutionPath
                Compiler-Build -Path $solutionPath -Targets $Targets -Configuration $Configuration
                $nuspecPath = Utility-SearchFile -Path $SourcesPath -Filter $Action.NuspecFilter
                NuGet-Package -NuspecPath $NuspecPath -NugetVersion $version.NuGetVersionV2 -Configuration $Configuration
            }
            # Version -> Restore NuGet -> Publish -> Package -> Restore Git
            3 { 

                $solutionPath = Utility-SearchFile -Path $SourcesPath -Filter $Action.SolutionFilter
                $version = Git-Version -SourcesPath $SourcesPath -UpdateAssemblyInfo "true"
                NuGet-Restore -Path $solutionPath
                $publish = Compiler-PublishLocal -Path $solutionPath -PublishProfile $Action.PublishProfile
                $nuspec = Utility-SearchFile -Path $publish.PublishPath -Filter $Action.NuspecFilter
                Utility-Cut -FromPath $nuspec -ToPath $publish.PublishPath
                $nuspec = Utility-SearchFile -Path $publish.PublishPath -Filter $Action.NuspecFilter

                NuGet-Package -NuspecPath $nuspec -NugetVersion $version.NuGetVersionV2 -Configuration $Configuration
            }
            #
            4 {
                $version = Git-Version -SourcesPath $SourcesPath -UpdateAssemblyInfo "false"
                $nuspec = Utility-SearchFile -Path $SourcesPath -Filter $Action.NuspecFilter
                NuGet-Package -NuspecPath $nuspec -NugetVersion $version.NuGetVersionV2 -Configuration $Configuration
            }
        }

        Write-Host "Restoring git..."
        Git-Restore -SourcesPath $SourcesPath

    }
}
catch
{
    Write-Host "---Exception occured---"
    Write-Host ""

    Write-Host $_.Exception.ToString()   
    Write-Host ""

    Write-Host "Restoring git..."
    Git-Restore -SourcesPath $SourcesPath
    Write-Host ""

    if (!$Local)
    {  
        Write-Host "##myget[buildProblem description='Build failed']"
        Write-Host ""
    }

    exit 1
}