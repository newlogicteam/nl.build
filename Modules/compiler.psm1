param(
    [parameter(Position = 0, Mandatory = $true)][boolean]$Local,
    [parameter(Position = 1, Mandatory = $true)][string]$AssetPath,
    [parameter(Position = 2, Mandatory = $true)][string]$RootPath
)

$ErrorActionPreference = "Stop"

[string]$MsBuildExe = ""
[string]$PublishPath = "$AssetPath\Publish"
[string]$BundlePath = "$PublishPath\Bundle"

function Init-Module
{
    # Create/Clean $PublishPath
    if(!(Test-Path $PublishPath -pathType container))
    {
        New-Item -ItemType Directory -Path $PublishPath
    }
    else
    {
        Get-ChildItem -Path $PublishPath -File | foreach { $_.Delete()}
    }

    # Create/Clean $BundlePath
    if(!(Test-Path $BundlePath -pathType container))
    {
        New-Item -ItemType Directory -Path $BundlePath
    }
    else
    {
        #Get-ChildItem -Path $PackagePath -Include * | foreach { $_.Delete() }
        Remove-Item "$BundlePath\*" -Recurse
    }

    # Setup variables
    if($Local -eq $true)
    {
        $Script:MsBuildExe = Get-MsBuildPath
    }
    else
    {
        If((Test-Path Env:\MsBuildExe) -eq $true) { $Script:MsBuildExe = $env:MsBuildExe }
    }
}

function Compiler-Build {
    param(
        [string]$Path,
        [string]$Targets,
        [string]$Configuration
    )

    # Build solution path with targets and properties
    & $MSBuildExe $Path /t:$Targets /p:"Configuration=$Configuration" /m | Out-Host
    if($LASTEXITCODE -ne 0)
    {
        Write-Error "MSBuild Build Error"
    }
}

function Compiler-Publish {
    param(
        $Path,
        $PublishProfile
    )

    & $MsBuildExe $Path /p:"DeployOnBuild=true" /p:"PublishProfile=$PublishProfile" /m | Out-Host
    if($LASTEXITCODE -ne 0)
    {
        Write-Error "MSBuild Publish Error"
    }
}

function Compiler-PublishLocal {
    param(
        $Path,
        $PublishProfile
    )

    & $MsBuildExe $Path /p:"DeployOnBuild=true" /p:"PublishProfile=$PublishProfile" /p:"publishUrl=$BundlePath" /m | Out-Host
    if($LASTEXITCODE -ne 0)
    {
        Write-Error "MSBuild PublishLocal Error"
    }

    return @{ PublishPath = $PublishPath; BundlePath = $BundlePath }
}

function Compiler-Information {
    Write-Host "--- Compiler Module Data ---"
    Write-Host "MsBuildExe: $MsBuildExe"
    Write-Host "PublishPath: $PublishPath"
}

function Get-MsBuildPath
{
<#
	.SYNOPSIS
	Gets the path to the latest version of MsBuild.exe. Throws an exception if MSBuild.exe is not found.
	
	.DESCRIPTION
	Gets the path to the latest version of MsBuild.exe. Throws an exception if MSBuild.exe is not found.

    .NOTES
	Copied from: https://invokemsbuild.codeplex.com/SourceControl/latest#Invoke-MsBuild.psm1
#>

	# Get the latest version of Visual Studio installed on this sytem.
	$VsVersion = Get-ChildItem 'HKLM:\SOFTWARE\Microsoft\VisualStudio\' | Where{$_ -match '[0-9].'} |sort pschildname -Descending | select -first 1 -ExpandProperty pschildname  

	# MsBuild is included with Visual Studio instead of .Net as of VS 2013 (v12.0), so need to look in .Net framework path if they don't have at least VS 2013 installed.
	if([version]$VsVersion -ge [version]"12.0")
	{
		$MsBuildVersion = $VsVersion
	}
	else
	{
		$MsBuildVersion = (Get-ChildItem 'HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP' | sort pschildname -Descending | select -first 1 -ExpandProperty pschildname).Substring(1)
	}

	# Get the path to the directory that MSBuild is in.
	$MsBuildDirectoryPath = ('HKLM:\SOFTWARE\Microsoft\MSBuild\ToolsVersions\{0}' -f $MsBuildVersion) | Get-ItemProperty -Name 'MSBuildToolsPath' | Select -ExpandProperty 'MSBuildToolsPath'

	if(!$MsBuildDirectoryPath)
	{
		throw 'MsBuild.exe was not found on the system.'          
	}

	# Get the path to the MSBuild executable.
	$MsBuildPath = (Join-Path -Path $MsBuildDirectoryPath -ChildPath 'msbuild.exe')

	if(!(Test-Path $MsBuildPath -PathType Leaf))
	{
		throw 'MsBuild.exe was not found on the system.'          
	}

	return $MsBuildPath
}

Init-Module

Export-ModuleMember Compiler-*