param(
    [parameter(Position = 0, Mandatory = $true)][boolean]$Local,
    [parameter(Position = 1, Mandatory = $true)][string]$AssetPath,
    [parameter(Position = 2, Mandatory = $true)][string]$RootPath,
    [parameter(Position = 3, Mandatory = $false)][string]$FeedAPIKey,
    [parameter(Position = 4, Mandatory = $false)][string]$FeedUrl
)

$ErrorActionPreference = "Stop"

[string]$PackagesPath = "$AssetPath\Packages"
[string]$NuGetUrl = "http://nuget.org/nuget.exe"
[string]$NuGet = "$AssetPath\NuGet.exe"

function Init-Module
{
    # Create PackagesPath if it does not exist.
    if(!(Test-Path $Script:PackagesPath -pathType container))
    {
        New-Item -ItemType directory -Path $Script:PackagesPath
    }

    if($Local -eq $true)
    {
        DownloadNuGet
    }
    else
    {
        If((Test-Path Env:\NuGet) -eq $true) { $Script:NuGet = $env:NuGet }
    }
}

function DownloadNuGet 
{
    # Download nuget if it does not exist
    if(!(Test-Path $NuGet -PathType Leaf))
    {
        (New-Object System.Net.WebClient).DownloadFile($NuGetUrl, $NuGet)
    }
}

function NuGet-Restore {
    param(
        [string]$Path
    )

    # Restore solution package
    & $NuGet restore $Path | Out-Host
    if($LASTEXITCODE -ne 0)
    {
        Write-Error "NuGet Restore Error"
    }
}

function NuGet-Package {
    param (
        [string]$NuspecPath,
        [string]$NugetVersion,
        [string]$Configuration
    )

    # Package the solution/nuspec
    & $NuGet pack $NuspecPath -Version $NugetVersion -IncludeReferencedProjects -OutputDirectory $PackagesPath -NoPackageAnalysis -Prop "Configuration=$Configuration" | Out-Host
    if($LASTEXITCODE -ne 0)
    {
        Write-Error "NuGet Package Error"
    }
}

function NuGet-Push {
    # Get latest nupkg package
    $latest = Get-ChildItem -Path $PackagesPath -Recurse -Filter "*.nupkg" | % { $_.FullName }
    
    # Push that package to feed
    foreach($file in $latest)
    {
        & $NuGet push $file $FeedAPIKey -Source $FeedUrl | Out-Host
        if($LASTEXITCODE -ne 0)
        {
            Write-Error "NuGet Push Error"
        }
    }

    if((Get-ChildItem $PackagesPath | Measure-Object).Count -gt 0)
    {
        Remove-Item -Path "$PackagesPath\*" -Recurse
    }

}

function NuGet-Information {
    Write-Host "--- NuGet Module Data ---"
    Write-Host "PackagesPath: $PackagesPath"
    Write-Host "NuGetUrl: $NuGetUrl"
    Write-Host "NuGet: $NuGet"
}

Init-Module

Export-ModuleMember Nuget-*