param (
    [string]$Location = "$((Get-Item $PSScriptRoot).Parent.FullName)\Bundle",
    [int]$Port = 44398
)

[string]$IISExpress = "${env:ProgramFiles(x86)}\IIS Express\iisexpress.exe"
[string]$AppCmd = "${env:ProgramFiles(x86)}\IIS Express\appcmd.exe"
[string]$ApplicationHostConfigPath = "${env:ProgramFiles(x86)}\IIS Express\AppServer\ApplicationHost.config"
[string]$ApplicationHostConfigCopyPath = "$PSScriptRoot\ApplicationHost.config"

try
{
    Copy-Item $ApplicationHostConfigPath -Destination $ApplicationHostConfigCopyPath

    . $AppCmd set config -section:system.ApplicationHost/sites /"[name='Development Web Site'].[path='/'].[path='/'].physicalPath:$Location" /commit:apphost /apphostconfig:$ApplicationHostConfigCopyPath

    . $AppCmd set config -section:system.ApplicationHost/sites /+"[name='Development Web Site'].bindings.[protocol='https',bindingInformation=':$($Port):localhost']" /commit:apphost /apphostconfig:$ApplicationHostConfigCopyPath

    . $AppCmd set config "Development Web Site" -section:system.webServer/directoryBrowse /enabled:"True" /commit:apphost /apphostconfig:$ApplicationHostConfigCopyPath

    . $IISExpress /config:$ApplicationHostConfigCopyPath /siteid:1 /systray:true
}
catch
{
    throw $_
}