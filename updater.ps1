[string]$Repository = "https://bitbucket.org/newlogicteam/nl.build/get/master.zip"
[string]$UnzipPath = "$PSScriptRoot\master.zip"
[string]$RepositoryPath = "$PSScriptRoot\master"

# Delete existing files, but exclude various local files/folders.
# -Exclude does not work for folder child items - therefore child items need to be specified in the where clause.
Get-ChildItem "$PSScriptRoot\*" -Recurse -Exclude "build.ps1" | 
                Where { $_.FullName -notlike "$PSScriptRoot\Local" -and $_.FullName -notlike "$PSScriptRoot\Local\*" } | 
                Remove-Item -Recurse -Force

# Download the source zip file
$webClient = New-Object System.Net.WebClient
$webClient.DownloadFile($Repository, $UnzipPath)

# Create windows shell to run unzip
$shell = New-Object -ComObject Shell.Application
# Get namespace
$zip = $shell.NameSpace($UnzipPath)
#Run through namespace items
$folder
foreach($item in $zip.items())
{
    $folder = $item.Name
    # Copy every item to folder
    $shell.Namespace("$PSScriptRoot").copyhere($item)
}

# Delete source zip file
Remove-Item $UnzipPath

if((Test-Path $RepositoryPath -pathType container))
{
    Remove-Item $RepositoryPath -Recurse
}

Rename-Item "$PSScriptRoot\$folder" "master"

# Copy items from master folder to root, but exclude local files/folders if they exist.
If((Test-Path "$PSScriptRoot\build.ps1" -PathType Leaf) -eq $true)
{
    Copy-Item "$RepositoryPath\*" $PSScriptRoot -Recurse -Exclude "build.ps1"
}
Else
{
    Copy-Item "$RepositoryPath\*" $PSScriptRoot -Recurse
}
