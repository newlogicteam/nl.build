﻿param(
    [parameter(Position = 0, Mandatory = $true)][boolean]$Local,
    [parameter(Position = 1, Mandatory = $true)][string]$AssetPath,
    [parameter(Position = 2, Mandatory = $true)][string]$RootPath
)

$ErrorActionPreference = "Stop"

[string]$GitPath = "$AssetPath\git.exe"
[string]$GitDependencyPath = "$AssetPath\libiconv-2.dll"
[string]$GitVersionZipPath = "$AssetPath\GitVersion.zip"
[string]$GitVersionUnzipPath = "$AssetPath\GitVersion\"
[string]$GitVersion = "$AssetPath\GitVersion.exe"

[string]$GitUrl = "https://github.com/github/msysgit/raw/PortableGit/bin/git.exe"
[string]$GitDependencyUrl = "https://github.com/github/msysgit/raw/PortableGit/bin/libiconv-2.dll"
[string]$GitVersionUrl = "http://chocolatey.org/api/v2/package/GitVersion.Portable"

function Init-Module
{
    if($Local -eq $true)
    {
        DownloadGitVersion
        DownloadGit
    }
    else
    {
        If((Test-Path Env:\GitVersion) -eq $true) { $Script:GitVersion = $env:GitVersion }
        If((Test-Path Env:\GitPath) -eq $true) { $Script:GitPath = $env:GitPath }
    }
}

function DownloadGitVersion
{

    # Download gitversion if it does not exist.
    if(!(Test-Path $GitVersion -PathType Leaf))
    {
        # Download GitVersion zip file if it does not already exist, and unzip.
        if(!(Test-Path $GitVersionZipPath -PathType Leaf))
        {
           (New-Object System.Net.WebClient).DownloadFile($GitVersionUrl, $GitVersionZipPath)
        }
        
        Utility-ZipExtract "$GitVersionZipPath" "$GitVersionUnzipPath"
       
        # Find GitVersion.exe from unzipped items.
        $gitVersionPath = Get-ChildItem -Path "$GitVersionUnzipPath\*" -Recurse -Filter "GitVersion.exe" | % { $_.FullName }
        Move-Item $gitVersionPath $AssetPath
    }

    # Cleanup
    if((Test-Path $GitVersionZipPath -PathType Leaf))
    {
        Remove-Item $GitVersionZipPath -Force
    }
    if((Test-Path $GitVersionUnzipPath -PathType Container))
    {
        Remove-Item $GitVersionUnzipPath -Recurse -Force
    }
}

function DownloadGit
{
    # Download git if it does not already exist
    if(!(Test-Path $GitPath -PathType Leaf))
    {
        (New-Object System.Net.WebClient).DownloadFile($GitUrl, $GitPath)
    }
    
    # Download git dependency file if does not already exist
    if(!(Test-Path $GitDependencyPath -PathType Leaf))
    {
        (New-Object System.Net.WebClient).DownloadFile($GitDependencyUrl, $GitDependencyPath)
    }
}

function Git-Version {
    param(
        [string]$SourcesPath,
        [string]$UpdateAssemblyInfo
    )

    # Get git repositories version and update solutions assemblyinfo
    [string]$json = & $GitVersion $SourcesPath /updateassemblyinfo $UpdateAssemblyInfo
    if($LASTEXITCODE -ne 0)
    {
        Write-Error "GitVersion Error"
    }

    # Convert returned data to an object
    $obj = $json | ConvertFrom-Json
    
    # Write it out to console
    Write-Host "GitVersion:"
    Write-Host ($obj | Out-String)

    # Return version object
    return $obj
}

function Git-Restore
{
    param(
        [string]$SourcesPath
    )
    Get-ChildItem -Path $SourcesPath -Include AssemblyInfo.cs -Recurse | % {
        & $GitPath checkout $_.FullName
        if($LASTEXITCODE -ne 0)
        {
            Write-Error "Git Checkout Error"
        }
    }
}

function Git-Root
{
    $path = & $GitPath rev-parse --show-toplevel
    if($LASTEXITCODE -ne 0)
    {
        Write-Error "Git TopLevel Error"
    }
    return $path
}

function Git-Information
{
    Write-Host "--- Git Module Data ---"
    Write-Host "GitPath: $GitPath"
    Write-Host "GitVersionZipPath: $GitVersionZipPath"
    Write-Host "GitVersionUnzipPath: $GitVersionUnzipPath"
    Write-Host "GitVersion: $GitVersion"
    Write-Host "GitVersionUrl: $GitVersionUrl"
    Write-Host "GitUrl: $GitUrl"
    Write-Host "GitDependencyUrl: $GitDependencyUrl"
    Write-Host "GitDependencyPath: $GitDependencyPath"
}

Init-Module

Export-ModuleMember Git-*