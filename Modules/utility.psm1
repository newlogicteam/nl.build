function Utility-SearchFile
{
    param(
        [string]$Path,
        [string]$Filter
    )
    
    return Get-ChildItem -Path $Path -Filter $Filter -Recurse | % { $_.FullName }
}

function Utility-Cut
{
    param(
        [string]$FromPath,
        [string]$ToPath
    )

    Copy-Item $FromPath $ToPath
    Remove-Item $FromPath
}

function Utility-ZipExtract
{
    [CmdletBinding()] 
    param ( 
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)] 
        [ValidateScript({ 
            if ((Test-Path -Path $_ -PathType Leaf) -and ($_ -like "*.zip")) { 
                $true 
            } 
            else { 
                Throw "$_ is not a valid zip file. Enter in 'c:\folder\file.zip' format" 
            } 
        })] 
        [string]$File, 
 
        [ValidateNotNullOrEmpty()] 
        [ValidateScript({ 
            if (Test-Path -Path $_ -PathType Container -IsValid) { 
                $true 
            } 
            else { 
                Throw "$_ is not a valid destination folder. Enter in 'c:\destination' format" 
            } 
        })] 
        [string]$Destination = (Get-Location).Path
    ) 

    # Create Destination folder if it does not exist
    if(!(Test-Path $destination -PathType Container))
    {
        New-Item -ItemType Directory -Path $destination
    }

    $shell = New-Object -ComObject Shell.Application 
    $shell.Namespace($destination).copyhere(($shell.NameSpace($file)).items()) 

}

Export-ModuleMember Utility-*