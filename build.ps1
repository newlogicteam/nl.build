﻿# Modes
# 0: Default - Nothing
# 1: NuGet Push latest package made.
# 2: Version -> Restore NuGet -> Build -> Package -> Restore Git
# 3: Version -> Restore NuGet -> Publish -> Package -> Restore Git
#
param (
    $Actions = 
        @( 
            @{ 
                Mode = 0; 
                PublishProfile = ""; 
                SolutionFilter = "*.sln"; 
                NuspecFilter = "*.nuspec"; 
                Files = @{ 
                   #@{ Source = "$PSScriptRoot\source\somefile.txt"; Target = "$PSScriptRoot\target\somefile.txt";}     
                } 
            } 

        ),
    # Push
    [string]$FeedAPIKey = "",
    [string]$FeedUrl = ""
)

& "$PSScriptRoot\Init\initializer.ps1" -Actions $Actions -FeedAPIKey $FeedAPIKey -FeedUrl $FeedUrl